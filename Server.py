#!/usr/bin/env python
# _*_ coding: utf8 _*_
import socket


def upserver():
    global server
    global ip  # ip y port de coneccion
    global target  # obj de coneccion
    server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    server.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    server.bind(('192.168.0.15', 7777))
    server.listen(1)
    print("Server Start...")
    target, ip = server.accept()
    print("Conecction recived to: " + str(ip[0]))


upserver()
server.close()
